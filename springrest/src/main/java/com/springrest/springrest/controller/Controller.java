package com.springrest.springrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springrest.springrest.Utilities.course;
import com.springrest.springrest.services.CourseService;

@RestController
public class Controller {
	@Autowired
	private CourseService obj;
	
	@GetMapping("/courses/{courseId}")
	public course getCourse(@PathVariable String courseId ) {
		System.out.print("as");
		return obj.getcoursebyid(courseId);
	}
	
	@GetMapping("/courses")
	public List<course> getList() {
		return obj.getcourse();
	}
	
	@PostMapping("/courses/add")
	public void addCourse(@RequestBody course c) {
		obj.setcourse(c);
	}
	
	
}
