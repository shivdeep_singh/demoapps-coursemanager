package com.springrest.springrest.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.springrest.springrest.Utilities.course;

@Service
public class CorServicesImple implements CourseService {
	List<course> list;
	
	public CorServicesImple() {
		super();
		list=new ArrayList<course>();
		list.add(new course(125,"Java"));
	}

	@Override
	public List<course> getcourse() {
		// TODO Auto-generated method stub
		
		return list;
	}
	
	@Override
	public course getcoursebyid(String courseId) {
		// TODO Auto-generated method stub
		for(course c:list) {
			if(c.getId()==Long.parseLong(courseId)) {
				return c;
			}
		}
		return new course(1,"sd");
	}

	@Override
	public void setcourse(course c) {
		// TODO Auto-generated method stub
		list.add(c);
	}
	
}
